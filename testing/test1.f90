MODULE test_mod

  use test_util
  USE CALENDAR_FUNCTIONS
  USE DATE_OPERATOR_INSTRUCTION
  !
  IMPLICIT NONE
  !
  PRIVATE
  PUBLIC:: test1
  !
  CONTAINS
  !
  !FUNCTION isANS(date,)
  !  type(date_operator), intent(inout):: date
  !  !
  !  call date%init('4/23/1979')
  !  !
  !END FUNCTION
  !
  SUBROUTINE test1(date)
    type(date_operator), intent(inout):: date
    !
    call date%init('4/23/1979')
    !
  END SUBROUTINE
  !
END MODULE