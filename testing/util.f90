MODULE test_util
  USE, INTRINSIC:: ISO_FORTRAN_ENV, ONLY: i8 => INT8,   i16 => INT16,  &
                                         i32 => INT32,  i64 => INT64,  &
                                         SNG => REAL32, DBL => REAL64, &
                                                        QAD => REAL128
  IMPLICIT NONE
  !---------------------------------------------------------------------------------------------------
  !
  REAL(DBL),     PARAMETER:: NaN    = TRANSFER(-2251799813685248_i64, 1._DBL)  ! This may not always be set to NaN, the safest method is to use "USE, INTRINSIC:: IEEE_ARITHMETIC, ONLY: IEEE_VALUE, IEEE_QUIET_NAN" and set a local variable to "NaN = IEEE_VALUE(NaN, IEEE_QUIET_NAN)"
  LOGICAL,       PARAMETER:: TRUE   = .TRUE.
  LOGICAL,       PARAMETER:: FALSE  = .FALSE.
  CHARACTER,     PARAMETER:: NL     = NEW_LINE(' ')
  CHARACTER(2),  PARAMETER:: BLN    = NL//NL
  CHARACTER,     PARAMETER:: BLNK   = ' '
  INTEGER(i32),  PARAMETER:: NINER  = -999
  INTEGER(i32),  PARAMETER:: NEG    = -1
  INTEGER(i32),  PARAMETER:: Z      =  0
  INTEGER(i32),  PARAMETER:: ONE    =  1
  REAL(DBL),     PARAMETER:: DNEG   = -1_dbl
  REAL(DBL),     PARAMETER:: DZ     =  0_dbl
  REAL(DBL),     PARAMETER:: UNO    =  1_dbl
  !
  PRIVATE
  !
  PUBLIC:: write_result   ! call write_result(test, id, iout, note)
  PUBLIC:: OpenTextWriter ! call OpenTextWriter(iu, fname)
  PUBLIC:: OpenTextReader ! call OpenTextReader(iu, fname)
  PUBLIC:: OpenBinWriter  ! call OpenBinWriter(iu, fname)
  PUBLIC:: OpenBinReader  ! call OpenBinReader(iu, fname)
  !
  PUBLIC:: isPass         ! check =isPass (val, ans, *[valX, ansX]) RESULT(isEqual)
  !
  PUBLIC:: PAUSE          ! call PAUSE(LINE)
  PUBLIC:: EPIC_FAIL      ! call EPIC_FAIL(iout)
  !
  INTERFACE isPass
     MODULE PROCEDURE isPass1
     MODULE PROCEDURE isPass2
     MODULE PROCEDURE isPass3
     MODULE PROCEDURE isPass4
     MODULE PROCEDURE isPass5
     MODULE PROCEDURE isPass15
  END INTERFACE
  !
  CONTAINS
  !
  SUBROUTINE write_result(test, id, iout, note)
    logical,      intent(in):: test
    integer,      intent(in):: id, iout
    character(*), intent(in):: note
    !
    if(test) then
        write(iout,'(I4,1x,A,5x,A)') id, 'PASS', note
    else
        write(iout,'(I4,1x,A,5x,A)') id, 'FAIL', note
    end if
    !
  END SUBROUTINE
  !
  SUBROUTINE OpenTextWriter(iu, fname)
    integer,      intent(out):: iu
    character(*), intent(in ):: fname
    !
    open(newunit=iu, file='date_operator_transcript.txt',  &
         action='WRITE', form='FORMATTED', status='REPLACE', &
         access='SEQUENTIAL', position='REWIND')
    !
  END SUBROUTINE
  !
  SUBROUTINE OpenTextReader(iu, fname)
    integer,      intent(out):: iu
    character(*), intent(in ):: fname
    !
    open(newunit=iu, file='date_operator_transcript.txt',  &
         action='READ', form='FORMATTED', status='OLD', &
         access='SEQUENTIAL', position='REWIND')
    !
  END SUBROUTINE
  !
  ! ######################################################################
  !
  SUBROUTINE OpenBinWriter(iu, fname)
    integer,      intent(out):: iu
    character(*), intent(in ):: fname
    !
    open(newunit=iu, file='date_operator_transcript.txt',  &
         action='WRITE', form='UNFORMATTED', status='REPLACE', &
         access='STREAM', position='REWIND')
    !
  END SUBROUTINE
  !
  SUBROUTINE OpenBinReader(iu, fname)
    integer,      intent(out):: iu
    character(*), intent(in ):: fname
    !
    open(newunit=iu, file='date_operator_transcript.txt',  &
         action='READ', form='UNFORMATTED', status='OLD', &
         access='STREAM', position='REWIND')
    !
  END SUBROUTINE
  !
  ! ######################################################################
  ! ##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@##
  ! ######################################################################
  !
  PURE FUNCTION isPass1(val, ans) RESULT(isEqual)
    class(*), intent(in):: val, ans
    LOGICAL:: isEqual
    !
    !
    SELECT TYPE (val)
    TYPE IS (CHARACTER(*))  
             SELECT TYPE (ans)
             TYPE IS (CHARACTER(*)); isEqual = val == ans
             END SELECT
    TYPE IS (REAL(DBL))  
             SELECT TYPE (ans)
             TYPE IS (REAL(DBL)); isEqual = val == ans
             END SELECT  
    TYPE IS (REAL(SNG))     
             SELECT TYPE (ans)
             TYPE IS (REAL(SNG)); isEqual = val == ans
             END SELECT   
    TYPE IS (INTEGER(i64))  
             SELECT TYPE (ans)
             TYPE IS (INTEGER(i64)); isEqual = val == ans
             END SELECT   
    TYPE IS (INTEGER(i32))     
             SELECT TYPE (ans)
             TYPE IS (INTEGER(i32)); isEqual = val == ans
             END SELECT
    TYPE IS (INTEGER(i16))     
             SELECT TYPE (ans)
             TYPE IS (INTEGER(i16)); isEqual = val == ans
             END SELECT
    TYPE IS (INTEGER(i8))     
             SELECT TYPE (ans)
             TYPE IS (INTEGER(i8)); isEqual = val == ans
             END SELECT
    TYPE IS (REAL(qad))    
             SELECT TYPE (ans)
             TYPE IS (REAL(qad)); isEqual = val == ans
             END SELECT
    CLASS DEFAULT
             isEqual = .FALSE.  !Unknown type always fails
    END SELECT
    !
  END FUNCTION
  !
  PURE FUNCTION isPass2(val1, ans1, val2, ans2) RESULT(isEqual)
    class(*), intent(in):: val1, ans1, val2 , ans2 
    LOGICAL:: isEqual
    !
                isEqual = isPass1(val1, ans1)
    if(isEqual) isEqual = isPass1(val2, ans2)
    !
  END FUNCTION
  !
  PURE FUNCTION isPass3(val1, ans1, val2, ans2, val3, ans3) RESULT(isEqual)
    class(*), intent(in):: val1, ans1, val2, ans2, val3, ans3
    LOGICAL:: isEqual
    !
                isEqual = isPass1(val1, ans1)
    if(isEqual) isEqual = isPass1(val2, ans2)
    if(isEqual) isEqual = isPass1(val3, ans3)
    !
  END FUNCTION
  !
  PURE FUNCTION isPass4(val1, ans1, val2, ans2, val3, ans3, val4, ans4) RESULT(isEqual)
    class(*), intent(in):: val1, ans1, val2, ans2, val3, ans3, val4, ans4
    LOGICAL:: isEqual
    !
                isEqual = isPass1(val1, ans1)
    if(isEqual) isEqual = isPass1(val2, ans2)
    if(isEqual) isEqual = isPass1(val3, ans3)
    if(isEqual) isEqual = isPass1(val4, ans4)
    !
  END FUNCTION
  !
  PURE FUNCTION isPass5(val1, ans1, val2, ans2, val3, ans3, val4, ans4, val5, ans5) RESULT(isEqual)
    class(*), intent(in):: val1, ans1, val2, ans2, val3, ans3, val4, ans4, val5, ans5
    LOGICAL:: isEqual
    !
                isEqual = isPass1(val1, ans1)
    if(isEqual) isEqual = isPass1(val2, ans2)
    if(isEqual) isEqual = isPass1(val3, ans3)
    if(isEqual) isEqual = isPass1(val4, ans4)
    if(isEqual) isEqual = isPass1(val5, ans5)
    !
  END FUNCTION
  !
  PURE FUNCTION isPass15(val1, ans1, val2, ans2, val3, ans3, val4, ans4, val5, ans5,  &
                         val6, ans6, val7, ans7, val8, ans8, val9, ans9, val10,ans10, &
                         val11,ans11,val12,ans12,val13,ans13,val14,ans14,val15,ans15  &
                        ) RESULT(isEqual)
    class(*), intent(in):: val1, ans1, val2, ans2, val3, ans3, val4, ans4, val5, ans5, val6,  ans6
    class(*), optional, intent(in):: val7,  ans7 
    class(*), optional, intent(in):: val8,  ans8 
    class(*), optional, intent(in):: val9,  ans9 
    class(*), optional, intent(in):: val10, ans10
    class(*), optional, intent(in):: val11, ans11
    class(*), optional, intent(in):: val12, ans12
    class(*), optional, intent(in):: val13, ans13
    class(*), optional, intent(in):: val14, ans14
    class(*), optional, intent(in):: val15, ans15
    LOGICAL:: isEqual
    !
                isEqual = isPass1(val1, ans1)
    if(isEqual) isEqual = isPass1(val2, ans2)
    if(isEqual) isEqual = isPass1(val3, ans3)
    if(isEqual) isEqual = isPass1(val4, ans4)
    if(isEqual) isEqual = isPass1(val5, ans5)
    if(isEqual) isEqual = isPass1(val6, ans6)
    !
    if(isEqual .AND. PRESENT(val7 )) isEqual = isPass1(val7, ans7)
    if(isEqual .AND. PRESENT(val8 )) isEqual = isPass1(val8, ans8)
    if(isEqual .AND. PRESENT(val9 )) isEqual = isPass1(val9, ans9)
    if(isEqual .AND. PRESENT(val10)) isEqual = isPass1(val10, ans10)
    if(isEqual .AND. PRESENT(val11)) isEqual = isPass1(val11, ans11)
    if(isEqual .AND. PRESENT(val12)) isEqual = isPass1(val12, ans12)
    if(isEqual .AND. PRESENT(val13)) isEqual = isPass1(val13, ans13)
    if(isEqual .AND. PRESENT(val14)) isEqual = isPass1(val14, ans14)
    if(isEqual .AND. PRESENT(val15)) isEqual = isPass1(val15, ans15)
    !
  END FUNCTION
  !
  ! ######################################################################
  ! ##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@##
  ! ######################################################################
  !
  SUBROUTINE PAUSE(LINE)
    USE, INTRINSIC:: ISO_FORTRAN_ENV, ONLY: STDIN=>INPUT_UNIT, STDOUT=>OUTPUT_UNIT
    CHARACTER(*), INTENT(IN), OPTIONAL:: LINE
    !
    IF(PRESENT(LINE)) THEN
        WRITE(STDOUT,'(/A/)') LINE
    ELSE
        WRITE(STDOUT,'(//A//)') 'PAUSED - Press Enter to Continue'
    END IF
    !
    READ(STDIN,*) 
    !
  END SUBROUTINE
  !
  ! ######################################################################
  ! ##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@##
  ! ######################################################################
  !
  !  EPIC_FAIL  routine
  !
  SUBROUTINE EPIC_FAIL(IOUT)
    INTEGER, INTENT(IN):: IOUT
    !
    IF(IOUT.NE.Z) WRITE(IOUT,'(///A)')                                                                     &
     "    .----------------.     .----------------.     .----------------.     .----------------. "//NL//  &
     "   | .--------------. |   | .--------------. |   | .--------------. |   | .--------------. |"//NL//  &
     "   | |  _________   | |   | |   ______     | |   | |     _____    | |   | |     ______   | |"//NL//  &
     "   | | |_   ___  |  | |   | |  |_   __ \   | |   | |    |_   _|   | |   | |   .' ___  |  | |"//NL//  &
     "   | |   | |_  \_|  | |   | |    | |__) |  | |   | |      | |     | |   | |  / .'   \_|  | |"//NL//  &
     "   | |   |  _|  _   | |   | |    |  ___/   | |   | |      | |     | |   | |  | |         | |"//NL//  &
     "   | |  _| |___/ |  | |   | |   _| |_      | |   | |     _| |_    | |   | |  \ `.___.'\  | |"//NL//  &
     "   | | |_________|  | |   | |  |_____|     | |   | |    |_____|   | |   | |   `._____.'  | |"//NL//  &
     "   | |              | |   | |              | |   | |              | |   | |              | |"//NL//  &
     "   | '--------------' |   | '--------------' |   | '--------------' |   | '--------------' |"//NL//  &
     "    '----------------'     '----------------'     '----------------'     '----------------' "//NL//  &
     " "//NL//  &
     "                                                  .----------------.     .----------------.     .----------------.     .----------------. "//NL//  &
     "                                                 | .--------------. |   | .--------------. |   | .--------------. |   | .--------------. |"//NL//  &
     "                                                 | |  _________   | |   | |      __      | |   | |     _____    | |   | |   _____      | |"//NL//  &
     "                                                 | | |_   ___  |  | |   | |     /  \     | |   | |    |_   _|   | |   | |  |_   _|     | |"//NL//  &
     "                                                 | |   | |_  \_|  | |   | |    / /\ \    | |   | |      | |     | |   | |    | |       | |"//NL//  &
     "                                                 | |   |  _|      | |   | |   / ____ \   | |   | |      | |     | |   | |    | |   _   | |"//NL//  &
     "                                                 | |  _| |_       | |   | | _/ /    \ \_ | |   | |     _| |_    | |   | |   _| |__/ |  | |"//NL//  &
     "                                                 | | |_____|      | |   | ||____|  |____|| |   | |    |_____|   | |   | |  |________|  | |"//NL//  &
     "                                                 | |              | |   | |              | |   | |              | |   | |              | |"//NL//  &
     "                                                 | '--------------' |   | '--------------' |   | '--------------' |   | '--------------' |"//NL//  &
     "                                                  '----------------'     '----------------'     '----------------'     '----------------' "//NL



  END SUBROUTINE
  !
END MODULE
    
    
  !
  !RECURSIVE PURE FUNCTION isPass11(val, ans, val1, ans1, val2, ans2, val3, ans3, val4, ans4, val5,   ans5, &
  !                                         val6, ans6, val7, ans7, val8, ans8, val9, ans9, val10, ans10  &
  !                              ) RESULT(isEqual)
  !  class(*),           intent(in):: val, ans
  !  class(*), optional, intent(in):: val1 , ans1 
  !  class(*), optional, intent(in):: val2 , ans2 
  !  class(*), optional, intent(in):: val3 , ans3 
  !  class(*), optional, intent(in):: val4 , ans4 
  !  class(*), optional, intent(in):: val5 , ans5 
  !  class(*), optional, intent(in):: val6 , ans6 
  !  class(*), optional, intent(in):: val7 , ans7 
  !  class(*), optional, intent(in):: val8 , ans8 
  !  class(*), optional, intent(in):: val9 , ans9 
  !  class(*), optional, intent(in):: val10, ans10
  !  LOGICAL:: isEqual
  !  !
  !  SELECT TYPE (val)
  !  TYPE IS (CHARACTER(*))  
  !           SELECT TYPE (ans)
  !           TYPE IS (CHARACTER(*)); isEqual = val == ans
  !           END SELECT
  !  TYPE IS (REAL(DBL))  
  !           SELECT TYPE (ans)
  !           TYPE IS (REAL(DBL)); isEqual = val == ans
  !           END SELECT  
  !  TYPE IS (REAL(SNG))     
  !           SELECT TYPE (ans)
  !           TYPE IS (REAL(SNG)); isEqual = val == ans
  !           END SELECT   
  !  TYPE IS (INTEGER(i64))  
  !           SELECT TYPE (ans)
  !           TYPE IS (INTEGER(i64)); isEqual = val == ans
  !           END SELECT   
  !  TYPE IS (INTEGER(i32))     
  !           SELECT TYPE (ans)
  !           TYPE IS (INTEGER(i32)); isEqual = val == ans
  !           END SELECT
  !  TYPE IS (INTEGER(i16))     
  !           SELECT TYPE (ans)
  !           TYPE IS (INTEGER(i16)); isEqual = val == ans
  !           END SELECT
  !  TYPE IS (INTEGER(i8))     
  !           SELECT TYPE (ans)
  !           TYPE IS (INTEGER(i8)); isEqual = val == ans
  !           END SELECT
  !  TYPE IS (REAL(qad))    
  !           SELECT TYPE (ans)
  !           TYPE IS (REAL(qad)); isEqual = val == ans
  !           END SELECT
  !  END SELECT
  !  !
  !  IF(isEqual .AND. PRESENT(val1 )) isEqual = isPass(val1, ans1)
  !  IF(isEqual .AND. PRESENT(val2 )) isEqual = isPass(val2, ans2)
  !  IF(isEqual .AND. PRESENT(val3 )) isEqual = isPass(val3, ans3)
  !  IF(isEqual .AND. PRESENT(val4 )) isEqual = isPass(val4, ans4)
  !  IF(isEqual .AND. PRESENT(val5 )) isEqual = isPass(val5, ans5)
  !  IF(isEqual .AND. PRESENT(val6 )) isEqual = isPass(val6, ans6)
  !  IF(isEqual .AND. PRESENT(val7 )) isEqual = isPass(val7, ans7)
  !  IF(isEqual .AND. PRESENT(val8 )) isEqual = isPass(val8, ans8)
  !  IF(isEqual .AND. PRESENT(val9 )) isEqual = isPass(val9, ans9)
  !  IF(isEqual .AND. PRESENT(val10)) isEqual = isPass(val10, ans10)
  !  !
  !END FUNCTION