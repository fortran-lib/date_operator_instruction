PROGRAM main
  USE, INTRINSIC:: ISO_FORTRAN_ENV, ONLY: STDIN=>INPUT_UNIT, STDOUT=>OUTPUT_UNIT, &
                                          i8 => INT8,   i16 => INT16,             &
                                         i32 => INT32,  i64 => INT64,             &
                                         SNG => REAL32, DBL => REAL64, QAD => REAL128
  !
  ! ----------------------------------
  use calendar_functions
  use date_operator_instruction
  ! ----------------------------------
  !
  !use test_mod
  use test_util
  !
  implicit none
  !
  type(date_operator):: date1, date2, date3, date4 
  integer:: iout, i, id
  logical:: test
  character(:), allocatable:: note
  !
  call OpenTextWriter(iout, 'date_operator_transcript.txt')
  !
  id   = 1
  test =.FALSE.
  note = 'This is a note to test the Note.'
  !
  write(iout,'(A)') "  ID TEST     NOTES"
  !
  call write_result(test, id, iout, note)
  !
  close(iout,iostat=id)
  call PAUSE('Fortran Program Paused. Press Enter to Continue')
  !
END PROGRAM main