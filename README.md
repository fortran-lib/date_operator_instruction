# Fortran Date and Time Operator

#### Repository Cloning Location

- `git clone https://gitlab.com/fortran-lib/date_operator_instruction.git`

Project Mirror:

- `git clone https://code.usgs.gov/fortran-lib/date_operator_instruction.git`

### Code Citation

Boyce, S.E., A Fortran Library for Date and Time Operations : ABC, [https://doi.org/10.3133/XYZ](https://doi.org/10.3133/tmXYZ)



## Abstract

Fortran 2008 Derived Data Type and subroutines that does basic calendar, date, and time operations. 

See the [Summary and Examples](#Summary and Examples) section for how to use or review `testing/main.f90 `on the `release` branch.

See the [Release Notes](#Release Notes) section on the `release` branch for a summary of version changes and features added.

Please see [LICENSE.md](LICENSE.md) for this repository’s license information.



## Git Branch Description

Please select one of the first three branches when working on the repository or cloning for your own personal use. 
It is recommended to use the `release` branch, as that contains all dependencies in the `dep` directory. 

If you only want the source code that pertains to this repository and plan to install the dependencies directly, 
then use the `source` branch. 

### release

Master Branch.

Contains full repository, which includes the directories `src`,  `ide`, `testing`, and uses git subtree to include dependencies in `dep`.

Also contains `makefile` and front matter (eg this file, README.md, and README.md)

### source

This branch is for conveyance when cloning as a subtree dependency 
and you do not want anything other than the main library code.

Only contains the source files located in `src`, which are moved to root of the repository.
`src/*` is the specific source code that pertains to this repository and not any of the driver code.

Important non-source files are included, but renamed to have `.date_operator_instruction` in the name.
For example, it contains `makefile` that only compiles the source to object code (.o) 
and the makefile is renamed to `makefile.date_operator_instruction`. 

### dev

Current unstable-development branch of `release`

### Any Other Branches

Any other branches are for hotfixes or utility programming and should be ignored.



## IDE Info

The `ide` directory holds input files for various integrated development environments. They are each kept in a separate folder identifying the IDE that is currently setup for the repo. The IDEs are not set up to be tracked with git (they are specified in `.gitignore`), but are included with the initial commit for convenience and use by people that clone the repository.

If using Visual Studio, then you should go into the specific folder (eg `visual_studio_2017`) and run the VS Solution File (`*.sln`) to start the ide.



## Summary and Examples

TBA



# Disclaimer

No warranty, expressed or implied, is made as to the functionality of the software repository and related material nor shall the fact of release constitute any such warranty. 

The software repository has been subjected to rigorous review. This repository, developers, and the organizations the developers belong to reserve the right to update the software as needed pursuant to further analysis and review. 

Furthermore, the software is released on condition that neither the developers nor the organizations the developers work for shall be held liable for any damages resulting from its authorized or unauthorized use.

#### License information

[LICENSE.md](LICENSE.md)



# Release Notes

##### v1.0.0

* Initial Release, version 1.0.0

