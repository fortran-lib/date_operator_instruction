#
# MAKEFILE for compiling date_operator_instruction 
#
# Developed by Scott E Boyce <seboyce@usgs.gov>
#
# https://www.usgs.gov/staff-profiles/scott-boyce
# https://gitlab.com/ScottBoyce
#
# PROVIDED AS IS WITHOUT WARRANTY OR HELP.
#
# If you use this makefile or library, please acknowledge by citing the code as:
#     Boyce, S.E., 2020, Fortran Date and Time Derived Data Type, version 1.0.0: U.S. Geological Survey Software Release, https://doi.org/10.YYY/XXX
#
# Also if you like, I would love to hear about how my libraries have helped you or how you are applying them.
#
# See next section to set the compiler type variables
#   The initial setup has the compiler type variables set to the 
#   Intel Fortran AND Intel C (ifort and icc) to make a RELEASE x64 version
#
# To Invoke/Compile type from command prompt:
# make                  Compile the src files and testing files. Binary located in testing/bin
# make run              Same as above, but runs the resulting test binary
# make clean            Delete all obejct (.o) and module (.mod) files
# make reset            Same as clean, except deletes all binaries.
#
#         
########################################################################
########################################################################
#     Set the following 3 Variables (OPTCFG, STATIC, F90)
########################################################################
########################################################################
#
# COMPILATION OPTIMIZATION SCHEME
# ===> Answer either "RELEASE" or "DEBUG"
OPTCFG := debug
#
# Define the Fortran compiler
# ===> ANSWER EITHER "ifort" or "gfortran"
#                  ****Note that the version of your Fortran compiler may not support all of the Fortran Standards (viz 2003, 2008, 2015)
F90 := gfortran
#
# DEFINE THE C COMPILER
# ===> ANSWER EITHER "icc" or "gcc"
CC  := gcc
#
# Program Name - Do not include extension
#
PROGRAM := date_operator
#
# To compile STATICALLY (No Library Dependence) 
# ===> answer either "YES" or "NO" to compile with STATIC option
STATIC := YES
#
# Specify if the compilation should force DOUBLE PRECISION for all REAL variables (REAL => DOUBLE PRECIONS)
# ===> answer either "YES" or "NO" 
#
DBLE := yes
#
#
########################################################################
########################################################################
#             DO NOT MODIFY BELOW THIS LINE
#            UNLESS YOU KNOW WHAT YOUR DOING
########################################################################
########################################################################
#
########################################################################
#
#Define final BIN, SOURCE, and Testing directories => Can be blank, but do not include the trailing forward slash /
#
bin  :=./testing/bin
src  :=./src
test :=./testing
#
# Location where test files are run
test_run:=./testing/test_run
#
# define the intermediate directory object and mod files are placed here
#
int:= ./testing/obj
#
#
#############################################################################
#
#Define the source code directory search path (really only need source directories)
#
VPATH:= $(src) $(test) $(int) $(bin)
#
#############################################################################
#
# Define the repository code to compile and the testing Fortran object files
#
main_src:= \
           $(src)/calendar_functions.f90          \
           $(src)/date_operator_instruction.f90
#
test_src:= \
           $(test)/util.f90   \
           $(test)/main.f90
#
src       := $(main_src) $(test_src)
#
obj:=  $(patsubst               %.f90, %.o,   \
         $(patsubst             %.f,   %.o,   \
           $(patsubst           %.fpp, %.o,   \
             $(patsubst         %.c,   %.o,   \
               $(patsubst       %.for, %.o,   \
                 $(patsubst     %.F,   %.o,   \
                   $(patsubst   %.F90, %.o,   \
                     $(patsubst %.FOR, %.o,   \
                            $(notdir $(src))  \
        ) ) ) ) ) ) ) )
#
#obj:= $(patsubst %.f90,%.o,$(patsubst %.f,%.o,$(src)))
########################################################################
#
# Check for if Windows or Unix
#
#
ifeq ($(OS),Windows_NT)
    ext:=.exe
else
    ext:=.nix
endif
#
########################################################################
#
# Remove blanks to ensure checks match to names
#
OPTCFG  :=$(strip $(shell echo $(OPTCFG) | tr [:lower:] [:upper:]))
STATIC  :=$(strip $(shell echo $(STATIC) | tr a-z A-Z))
DBLE    :=$(strip $(shell echo $(DBLE)   | tr a-z A-Z))
F90     :=$(strip $(F90))
CC      :=$(strip $(CC))
PROGRAM :=$(strip $(PROGRAM))
#
########################################################################
#
# Set up names and optimizations depending on compiler and configuration
#
ifeq ($(OPTCFG), DEBUG)
   #
   PROGRAM:=$(PROGRAM)_debug
   #
   F90FlagsIntel :=-O0 -g -debug -traceback -assume nocc_omp -fpe0 -fp-model source -nologo -warn nousage -check bounds,pointers,stack,format,output_conversion,uninit
   F90FlagsGCC   :=-O0 -g -fdefault-double-8  -ffree-line-length-2048 -std=f2008 -fmax-errors=10 -w #-fstack-usage  #<= THIS PROVIDES LOTS OF INFO
   #
   #CFlagsIntel  =-O0 -debug -g  -fbuiltin 
   #CFlagsGCC    =-O0        -g  -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast
else
   # NOTE "-ip" can sometimes cause catastrophic error: **Internal compiler error:
   #
   F90FlagsIntel :=-O2 -real-size 64 -assume nocc_omp -fpe0 -fp-model source -threads -warn nousage -nologo
   F90FlagsGCC   :=-O2 -fno-backtrace -fdefault-real-8 -fdefault-double-8 -ffree-line-length-2048
   #
   #CFlagsIntel  =-O2  -fbuiltin
   #CFlagsGCC    =-O2  -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast
endif
#
########################################################################
#
# Check if DBLE == YES to add default REAL-8 options
#
ifeq ($(DBLE), YES)
   #
   F90FlagsIntel+= -real-size 64                        
   F90FlagsGCC  += -fdefault-real-8
endif
#
########################################################################
#
# Establish Proper Optimization Flags and Static Flags
#
# Ensure that variables are set if STATIC not in use
ifneq ($(strip $(STATIC)), YES)
  STATIC:=
  STATICLNK:=
endif
#
# STATICLNK+= -static-libgfortran  => This can cause problems with gcc runtime so best not to use
ifeq ($(F90), gfortran)
  mod:=-J $(int)
  F90FLAGS:=$(F90FlagsGCC)
  ifeq ($(strip $(STATIC)), YES)
    STATIC   :=-static -static-libgfortran -static-libgcc  -static-libstdc++
    STATICLNK:=-static -static-libgfortran -static-libgcc  -static-libstdc++
  endif
endif
#
ifeq ($(F90), ifort)
  mod:=-module $(int)
  F90FLAGS:=$(F90FlagsIntel)
  ifeq ($(strip $(STATIC)), YES)
    STATIC   :=-static -static-intel -qopenmp-link=static -static-libstdc++ -static-libgcc
    STATICLNK:=-static -static-intel -qopenmp-link=static -static-libstdc++ -static-libgcc
  endif
endif
#
########################################################################
#
# Remove Variable Blank Space For Cleaner Output
#
F90FLAGS :=$(strip $(F90FLAGS))
STATIC   :=$(strip $(STATIC))
STATICLNK:=$(strip $(STATICLNK))
#
########################################################################
#
#SET UP PROGRAM NAME
#
PROG:=$(strip $(bin)/$(PROGRAM)$(ext))
#
###########################################################################
###########################################################################
#     DEFINE ALL TASK FUNCTIONS                                         ###
#     THE FOLLOW TARGETS EVALUATE THE COMPILATION                       ###
###########################################################################
###########################################################################
#
#
all: 
	@$(MAKE) --no-print-directory runMake || $(MAKE) --no-print-directory errorClean
#
run: 
	@$(MAKE) --no-print-directory runTest || $(MAKE) --no-print-directory errorClean
#
runMake: startMSG preclean $(PROG)
#
runTest: runMake
	@echo; echo;
	@echo "Now running program to evaluate tests."
	@echo; echo;
	cd $(test_run) ; $(abspath $(PROG))
	@echo; echo; echo
	@echo "   MAKEFILE TESTS COMPLETE"
	@echo; echo
#
startMSG:
	@echo; echo
	@echo "                 $(OPTCFG) COMPILATION"
	@echo
	@echo "                        OF"
	@echo
	@echo "                $(PROGRAM)"
	@echo; echo
#          
$(PROG): $(addprefix $(int)/,$(obj))
	@echo
	@echo "OBJECTS HAVE BEEN CREATED NOW LINKING FINAL BINARY:"
	@echo 
	@echo "                              $(PROG)"
	@echo
	@$(F90) $(F90FLAGS) $(mod)   $^   $(STATICLNK)  -o  $@
	@echo; echo
	@echo "   MAKEFILE COMPILATION COMPLETE"
	@echo; echo
#
preclean:
	@rm -rf $(PROG) 
#
clean: 
	@echo
	find . -name '*.o'    -delete
	find . -name '*.mod'  -delete
	find . -name '*.smod' -delete
	@echo; echo
#
errorClean: 
	@echo; echo; echo
	@echo "                 $(OPTCFG) $(PROGRAM) MAKEFILE FAILED"
	@echo
	@echo "            Cleaning up intermediate files..."
	@echo; echo
	@rm -rf $(PROG) 
	@echo; echo
#
reset: 
	@echo
	find . -name '*.o'    -delete
	find . -name '*.mod'  -delete
	find . -name '*.smod' -delete
	rm -rf $(bin)*.exe
	rm -rf $(bin)*.nix
	@echo; echo
#
#
#################################################################################
###  Object Code Recipies  ######################################################
#################################################################################
#
$(int)/%.c : %.c
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(CC) $(CFLAGS) $(STATIC)  -c  $<  -o $@
#
$(int)/%.o : %.f
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(F90) $(F90FLAGS) $(mod) $(STATIC)  -c  $<  -o $@
#
$(int)/%.o : %.F
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(F90) $(F90FLAGS) $(mod) $(STATIC)  -c  $<  -o $@
#
$(int)/%.o : %.f90
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(F90) $(F90FLAGS) $(mod) $(STATIC)  -c  $<  -o $@
#
$(int)/%.o : %.F90
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(F90) $(F90FLAGS) $(mod) $(STATIC)  -c  $<  -o $@
#
$(int)/%.o : %.fpp
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(F90) $(F90FLAGS $(mod)) $(STATIC)  -c  $<  -o $@
#
$(int)/%.o : %.for
	@echo "Compiling   $(notdir $<)"
	@echo
	@$(F90) $(F90FLAGS) $(mod) $(STATIC)  -c  $<  -o $@
#

#
###############################################################################
#
# Setup suffixes that will be processes # .h .mod
.SUFFIXES: .o .f .for .F .FOR .f90 .F90 .fpp .c 
#
# #############################################################################
#
# Phony Targets
.PHONY: all run clean reset preclean errorClean startMSG runMake runTest
#
#
# THE END #####################################################################